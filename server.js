const express = require('express')

const app = express()

app
    .get('/aklanon', (request, response) => {
        response
        .set({'Content-Type': 'text/html'})
        .send (
            `<html>
                <head>
                <title> Sean's World </title>
                </head>
                <body>
                    <h1> Sean's Cool Web </h1>
                    ${new Date().toLocaleString()}
                </body>
            </html> 
            `
        )
        .end()
    })
    .listen(8000, () => {
        console.log('Server has been started at http://localhost:8000')
    })